package Binary;

import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import viever.DisplayImage;
import viever.Mat2BufferedImage;
import viever.Resizer;


public class BinaryImg {

	public static Mat bin; 
	public static Vector<Integer> pix;
	
	//1 - var dla tylko czesci obrazu, 2 - dla calosci 
	public static double[] makeBin(Mat src, char variant) throws IOException{
		
		Mat grayMat = new Mat();
		Imgproc.cvtColor(src,  grayMat, Imgproc.COLOR_RGB2GRAY);
		Resizer.changeSize(src, grayMat);
		Mat bin = new Mat(src.rows(),src.cols(),src.type());
		
		Imgproc.threshold(grayMat,bin,170,255,Imgproc.THRESH_BINARY);
		//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(bin));
		
		double[] dsr = countPix(bin, variant);
		closeImg(bin);
		
		return dsr;
	}
	public static double[] countPix(Mat bin, char variant){
		
		Vector<Integer> pixHori = new Vector<Integer>();
		Vector<Integer> pixVert = new Vector<Integer>();
		double[] dsr = new double[2]; 
		int pomV = 0;
		int pomH = 0; 
		//poziomo
		for(int i = 0; i<bin.rows(); i++){
			for(int j = 0; j<bin.cols();j++){
				if(bin.get(i,j)[0]==0.0)
					pomH = pomH+1;
			}
			pixHori.add(pomH);
			pomH = 0;
			//System.out.println("");
		}
		//pionowo
		for(int i = 0; i<bin.cols(); i++){
			for(int j = 0; j<bin.rows();j++){
				if(bin.get(j,i)[0]==0.0)
					pomV = pomV +1;
			}
			pixVert.add(pomV);
			pomV = 0; 
			//System.out.println("");
		}
		
		if(variant == 1 ){
			dsr[0] = average(pixHori, '1');
			dsr[1] = average(pixVert, '1');
		}
		else {
			dsr[0] = average(pixHori, '2');
			dsr[1] = average(pixVert, '2');
		} 
		
/*		System.out.println("ilosc pixeli:");
		for(Integer h : pixHori)
			System.out.print(h + ",");
		System.out.println("");
		for(Integer v : pixVert)
			System.out.print(v + ",");
		
		System.out.println("srednie:");
		System.out.println(dsr[0] +", "+dsr[1]);
	*/	
		return dsr;
	}
	//dylatacja a potem erozja
	public static void closeImg(Mat bin) throws IOException{
		
		Mat closeImg = new Mat();
		//Imgproc.dilate(bin, closeImg, new Mat());
	//	DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(closeImg));
		Imgproc.erode(bin, closeImg, new Mat());
	//	DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(closeImg));
	}
	//1 - variant pierwszy srednia z tego gdzie sa pixele, 2 - na calosci 
	public static double average(Vector<Integer> pix, char variant){
		
		double pom = 0;
		double noneZero = 0;
		for(int i = 0; i<pix.size(); i++){
			pom = pom + pix.get(i);
			if (pix.get(i)!=0)
				noneZero++;
		}
		if( variant == '1')
			return (pom/noneZero);
		else return (pom/pix.size());
	}
}
