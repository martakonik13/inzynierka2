package Binary;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;

import viever.FileWeight;
import viever.LoadImage;

import viever.Searcher;
import viever.Sort;

public class BinaryImageMethod {

	public static Vector<File> met(Mat src, Vector<File> files) throws IOException{

		double[] dsrSrc1 = BinaryImg.makeBin(src,'1');
		double[] dsrSrc2 = BinaryImg.makeBin(src,'2');
		//tutaj dorobic to samo dla drugiego sposobu liczenia sredniej
		//majac 4 liczby z 1 i 2 porownuje obie i robie roznice
		//nastepnie srednia?
		File[] locations = Searcher.findLocations();
		Vector <File> similarVec = new Vector<File>();
		FileWeight[] similarImgs = new FileWeight[files.size()];

		int index = 0; 

		Mat dbMat = new Mat();
		for( File loc: files){
			double pom = 0;
			double pomX = 0;
			//	System.out.println(loc.getName());
			dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			double[] dsrImg1 = BinaryImg.makeBin(dbMat,'1');
			double[] dsrImg2 = BinaryImg.makeBin(dbMat,'2');
			//srednia 2 odleglosci 
			pom = Math.abs(dsrSrc1[0] - dsrImg1[0]) + Math.abs(dsrSrc1[1] - dsrImg1[1]);
			pomX = Math.abs(dsrSrc1[1] - dsrImg1[0]) + Math.abs(dsrSrc1[0] - dsrImg1[1]);
			//	System.out.println("pom = "+ pom +" , pomX= "+ pomX);
			//
			double weight1 = Math.min(pom, pomX);
			//srednia drugich dwoch
			pom = Math.abs(dsrSrc2[0] - dsrImg2[0]) + Math.abs(dsrSrc2[1] - dsrImg2[1]);
			pomX = Math.abs(dsrSrc2[1] - dsrImg2[0]) + Math.abs(dsrSrc2[0] - dsrImg2[1]);
			//	System.out.println("pom = "+ pom +" , pomX= "+ pomX);
			//FileWeight fileW2 = new FileWeight (loc,
			double weight2 = Math.min(pom, pomX);


			FileWeight fileW = new FileWeight (loc,(weight1+weight2)/2);
			similarImgs[index] = fileW;
			index++;
		}
		similarVec = Sort.sort(similarImgs);
		
		System.out.println(similarImgs.length + ", posegeregowne: ");

		for(File fi : similarVec)
			System.out.println(fi.getName());
		
		Vector<File> noSimilarVec = new Vector<File>();
		
		int max;
		if(similarVec.size()>50)
			max = (similarVec.size()-16);
		else if (similarVec.size()>30)
			 max = (similarVec.size()-11);
		else 
			max = similarVec.size()-1;  
		//System.out.println("rozm:" + similarVec.size()+ ", -40: "+ (similarVec.size()-40));
		for(int j = (similarVec.size()-1); j> max; --j){	
			//System.out.println("i: " + i + ", "+ similarVec.get(i).getName());
			noSimilarVec.add(similarVec.get(j));
			similarVec.remove(j);
		}
		
		//wyswietlam co odrzycam: 
		System.out.println("metoda binary odpada: " + noSimilarVec.size() );
	//	for(File f: noSimilarVec)
	//		System.out.println(f.getName());
		
		int b =0, c =0, j=0, m=0, p=0, pom=0, t =0; 
		for(int ji = 0; ji<noSimilarVec.size(); ji++){
			if(noSimilarVec.get(ji).getName().contains("ban")==true)
				b++;
			if(noSimilarVec.get(ji).getName().contains("carrot")==true)
				c++;
			if(noSimilarVec.get(ji).getName().contains("jab")==true)
				j++;
			if(noSimilarVec.get(ji).getName().contains("mini")==true)
				m++;
			if(noSimilarVec.get(ji).getName().contains("pilka")==true)
				p++;
			if(noSimilarVec.get(ji).getName().contains("pom")==true)
				pom++;
			if(noSimilarVec.get(ji).getName().contains("trus")==true)
				t++;
		}
		System.out.println("zosta�o: " + similarVec.size() + " binary sift odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", m: " + m +", p: " + p +", pom: " + pom +", t: " + t );
		

		return similarVec;
	}
}

