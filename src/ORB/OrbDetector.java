package ORB;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import Brief.BriefDetector;
import viever.FileWeight;
import viever.LoadImage;
import viever.Resizer;
import viever.Searcher;
import viever.Sort;

public class OrbDetector {


	public ArrayList<MatOfKeyPoint> keypoints;

	public Mat detect(Mat src) {
		// TODO Auto-generated method stub
		// do detekcji cech
		FeatureDetector orbFeatDetector = FeatureDetector
				.create(FeatureDetector.ORB);
		//wyciagam cechy
		DescriptorExtractor orbDsrExtractor = DescriptorExtractor
				.create(DescriptorExtractor.ORB); 
		
		keypoints = new ArrayList<MatOfKeyPoint>(); 
		
		keypoints.add(new MatOfKeyPoint());

		Mat dsr = new Mat();

		Mat grayMat = new Mat();
		Imgproc.cvtColor(src,  grayMat, Imgproc.COLOR_RGB2GRAY);
		
		orbFeatDetector.detect(grayMat, keypoints.get(0));
		orbDsrExtractor.compute(grayMat, keypoints.get(0), dsr);

		return dsr;
	}

}

