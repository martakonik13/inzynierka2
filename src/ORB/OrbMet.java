package ORB;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.Scalar;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;

import viever.Mat2BufferedImage;
import viever.DisplayImage;
import Brief.BriefDetector;
import viever.FileWeight;
import viever.LoadImage;
import viever.Resizer;
import viever.Searcher;
import viever.Sort;

public class OrbMet {

	public static Vector<File> met(Mat src, Vector<File> files) throws IOException{
		//	File[] locations = Searcher.findLocations();
		Vector <File> similarVec = new Vector<File>();
		Vector <File> noSimilarVec = new Vector<File>();
		Vector<FileWeight> similarImgs = new Vector<FileWeight>();

		DescriptorMatcher bruteDescriptorMatcherK = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);

		ArrayList<MatOfDMatch> matches = new ArrayList<MatOfDMatch>();
		matches.add(new MatOfDMatch());

		BriefDetector srcS = new BriefDetector();
		Mat dsrSrc = srcS.detect(src);

		for ( File loc: files){

			Mat dbMat = LoadImage.loadImage(loc.getAbsolutePath());

			BriefDetector srcD = new BriefDetector();
			Mat dsrDb = srcD.detect(dbMat);
			//	bruteDescriptorMatcherK.match(dsrSrc, dsrDb, matches.get(0));
			bruteDescriptorMatcherK.knnMatch(dsrSrc, dsrDb, matches, 5);

	//		System.out.println(loc.getName());
			Double rozm = srcS.keypoints.get(0).size().height;
	//		System.out.println(rozm + ", " + srcD.keypoints.get(0).size().height);
		
			if(rozm<120 && (Math.abs(rozm - srcD.keypoints.get(0).size().height)>650)){
					noSimilarVec.add(loc);
					System.out.println("kompletnie nie podobne: "+loc.getName()); 
				}
			else if(rozm >1000 && srcD.keypoints.get(0).size().height<120){
				noSimilarVec.add(loc);
				System.out.println("kompletnie nie podobne: "+loc.getName()); 
			}
			else{
			List<DMatch> listD = new ArrayList<DMatch>();
			listD = matches.get(0).toList();

			//	System.out.println("odleglosci");
			//	for(int k = 0; k<listD.size(); k++)
			//		System.out.println(listD.get(k).distance);
		
			//sortuje matches - babelki ?

			for(int i = 0; i<listD.size(); i++){
				for(int j = 0; j<listD.size()-1; j++){
					if(listD.get(j).distance>listD.get(j+1).distance){
						DMatch temp = listD.get(j);
						DMatch temp2 = listD.get(j+1);
						listD.set(j,temp2);
						listD.set(j+1, temp);
					}
				}
			}
/*			System.out.println("odleglosci posortowane");
			for(int k = 0; k<listD.size(); k++)
				System.out.println(listD.get(k).distance);			
*/
			Double dist = 0.0;
			for(DMatch d : listD){
				dist = dist + d.distance;
			}

		//	System.out.println(loc.getName() +" "+ dist);

			FileWeight fileW= new FileWeight(loc, dist);
			similarImgs.add(fileW);
			}
		}
		similarVec = Sort.sort(similarImgs);

	/*	System.out.println("posegeregowne: orb");
		int in = 1; 
		for(File fi : similarVec){
			System.out.println(in + fi.getName());
			in ++;
		}
	*/
		int max = similarImgs.size(); 
		if(similarImgs.size()>80&&similarImgs.size()<100)
			max = similarImgs.size() - 7;
		else if(similarImgs.size()>100&&similarImgs.size()<110)	
			max = similarImgs.size() - 10;
		else if(similarImgs.size()>110)
			max = similarImgs.size() - 21;

		for(int j = (similarVec.size()-1); j> max; --j){	
			noSimilarVec.add(similarVec.get(j));
			similarVec.remove(j);
		}

	//wyswietlam co odrzycam: 
		System.out.println("metoda orb: " + noSimilarVec.size() );
		
		int b =0, c =0, j=0, p=0, pom=0, t =0; 
		for(int ji = 0; ji<noSimilarVec.size(); ji++){
			if(noSimilarVec.get(ji).getName().contains("ban")==true)
				b++;
			if(noSimilarVec.get(ji).getName().contains("carrot")==true)
				c++;
			if(noSimilarVec.get(ji).getName().contains("jab")==true)
				j++;
			if(noSimilarVec.get(ji).getName().contains("pilka")==true)
				p++;
			if(noSimilarVec.get(ji).getName().contains("pom")==true)
				pom++;
			if(noSimilarVec.get(ji).getName().contains("trus")==true)
				t++;
		}
		System.out.println("zosta�o: " + similarVec.size() + " orb odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", p: " + p +", pom: " + pom +", t: " + t );
		
		
		
		return similarVec;
	}
	
}
