package Brief;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

public class BriefDetector {

	public class BriefDetect {

		public ArrayList<MatOfKeyPoint> keypoints = new ArrayList<MatOfKeyPoint>();

		public Mat detect(Mat src) {
			return null;
		
		}
	}

	public ArrayList<MatOfKeyPoint> keypoints;

	public Mat detect(Mat src) {
		// TODO Auto-generated method stub
		// do detekcji cech
		FeatureDetector fastFeatDetector = FeatureDetector
				.create(FeatureDetector.FAST);
		//wyciagam cechy
		DescriptorExtractor briefDsrExtractor = DescriptorExtractor
				.create(DescriptorExtractor.BRIEF); 
		
		keypoints = new ArrayList<MatOfKeyPoint>(); 
		
		keypoints.add(new MatOfKeyPoint());

		Mat dsr = new Mat();

		Mat grayMat = new Mat();
		Imgproc.cvtColor(src,  grayMat, Imgproc.COLOR_RGB2GRAY);
		
		fastFeatDetector.detect(grayMat, keypoints.get(0));
		briefDsrExtractor.compute(grayMat, keypoints.get(0), dsr);

		return dsr;
	}

	
}
