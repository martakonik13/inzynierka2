package Brief;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.Scalar;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;

import Brief.BriefDetector;
import viever.DisplayImage;
import viever.FileWeight;
import viever.LoadImage;
import viever.Mat2BufferedImage;
import viever.Resizer;
import viever.Searcher;
import viever.Sort;

public class BriefMet {

	public static Vector<File> met(Mat src, Vector<File> files) throws IOException{
		File[] locations = Searcher.findLocations();
		Vector <File> similarVec = new Vector<File>();
		Vector <File> noSimilarVec = new Vector<File>();
		Vector<FileWeight> similarImgs = new Vector<FileWeight>();

		DescriptorMatcher bruteDescriptorMatcherK = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);

		ArrayList<MatOfDMatch> matches = new ArrayList<MatOfDMatch>();
		matches.add(new MatOfDMatch());

		BriefDetector srcS = new BriefDetector();
		Mat dsrSrc = srcS.detect(src);

		for ( File loc: files){
			Mat dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			//System.out.println(loc.getName());
			Mat resDb = Resizer.changeSize(src, dbMat);

			BriefDetector srcD = new BriefDetector();
			Mat dsrDb = srcD.detect(resDb);
			//bruteDescriptorMatcherK.match(dsrSrc, dsrDb, matches.get(0));
			bruteDescriptorMatcherK.knnMatch(dsrSrc, dsrDb, matches, 1);

			Double rozm = srcS.keypoints.get(0).size().height;
			//System.out.println(rozm + ", " + srcD.keypoints.get(0).size().height);
			if((rozm>50)&&(rozm<430)&&(Math.abs(rozm - srcD.keypoints.get(0).size().height)>2550)){
				noSimilarVec.add(loc);
			}
			else if((rozm<50)&&(srcD.keypoints.get(0).size().height>450)){
				noSimilarVec.add(loc);
			}
			else if((rozm>450)&&(srcD.keypoints.get(0).size().height<50)){
				noSimilarVec.add(loc);
			}
			else{
				//System.out.println(loc.getName());
				List<DMatch> listD = new ArrayList<DMatch>();
				listD = matches.get(0).toList();

				Double dist = 0.0;
				for(DMatch d : listD){
					dist = dist + d.distance;
				}

				Double result= (dist/srcD.keypoints.get(0).size().height);
				//System.out.println(result);

				FileWeight fileW= new FileWeight(loc, result);
				similarImgs.add(fileW);

				/*
				Mat outImg = new Mat();

				Features2d.drawMatches(src, srcS.keypoints.get(0), resDb,
						srcD.keypoints.get(0), matches.get(0), outImg,
						new Scalar(0, 255, 0), new Scalar(0, 0, 255), new MatOfByte(),
						Features2d.NOT_DRAW_SINGLE_POINTS);
				 */
				//Features2d.drawMatches2(src, srcS.keypoints.get(0),resDb, srcD.keypoints.get(0), matches, outImg);

				//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(outImg));
			}

		}
		similarVec = Sort.sort(similarImgs);
		
	//	System.out.println("metoda brief: przed odrzuceniem dodatkowym : " + noSimilarVec.size() );
/*		
		System.out.println("posegeregowne: ");
		int in = 1; 
		for(File fi : similarVec){
			System.out.println(in + fi.getName());
			in ++;
		}
		*/
		int max =similarImgs.size(); 
		if(similarImgs.size()>80&&similarImgs.size()<100)
			max = similarImgs.size() - 5;
		else if(similarImgs.size()>100 && similarImgs.size()<120)	
			max = similarImgs.size() - 10;
		else if(similarImgs.size()>120)
			max = similarImgs.size() - 40;

		for(int j = (similarVec.size()-1); j> max; --j){	
		//	System.out.println("i: " + j + ", "+ similarVec.get(j).getName());
			noSimilarVec.add(similarVec.get(j));
			similarVec.remove(j);
		}
/*	
	//wyswietlam co odrzycam: 
		System.out.println("metoda brief: " + noSimilarVec.size() );
	for(File f: noSimilarVec)
		System.out.println(f.getName());
*/
		
		int b =0, c =0, j=0, p=0, pom=0, t =0; 
		for(int ji = 0; ji<noSimilarVec.size(); ji++){
			if(noSimilarVec.get(ji).getName().contains("ban")==true)
				b++;
			if(noSimilarVec.get(ji).getName().contains("carrot")==true)
				c++;
			if(noSimilarVec.get(ji).getName().contains("jab")==true)
				j++;
			if(noSimilarVec.get(ji).getName().contains("pilka")==true)
				p++;
			if(noSimilarVec.get(ji).getName().contains("pom")==true)
				pom++;
			if(noSimilarVec.get(ji).getName().contains("trus")==true)
				t++;
		}
		System.out.println("zosta�o: " + similarVec.size() + " brief odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", p: " + p +", pom: " + pom +", t: " + t );
		
	return similarVec;

}
}
