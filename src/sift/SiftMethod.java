package sift;

import java.util.ArrayList;
import java.util.Vector;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.lang.Object;
import java.lang.reflect.Array;

import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import viever.DisplayImage;
import viever.FileWeight;
import viever.LoadImage;
import viever.Mat2BufferedImage;
import viever.Resizer;
import viever.SaveDsr;
import viever.Searcher;
import viever.Sort;

import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.highgui.Highgui;
public class SiftMethod {

	public static Vector<File> met(Mat src)throws IOException{// Vector<File> files) throws IOException{
		
		Vector <File> similarVec = new Vector<File>();
		Vector <File> noSimilarVec = new Vector<File>();
		Vector<FileWeight> similarImgs = new Vector<FileWeight>();
		File[] locations = Searcher.findLocations();
		
		//DescriptorMatcher bruteDescriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);
		DescriptorMatcher bruteDescriptorMatcherK = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);
		ArrayList<MatOfDMatch> matches = new ArrayList<MatOfDMatch>();
		matches.add(new MatOfDMatch());

		SiftDetect srcS = new SiftDetect();
		Mat dsrSrc = srcS.detect(src);

		//System.out.println(srcS.keypoints.get(0).size().height);
		for ( File loc: locations){
			Mat dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			//System.out.println(loc.getName());
		
			//sprawdz czy mam juz deskryptor 
			
			SiftDetect srcD = new SiftDetect();
			Mat dsrDb = srcD.detect(dbMat);
			//bruteDescriptorMatcher.match(dsrSrc, dsrDb, matches.get(0));
			bruteDescriptorMatcherK.knnMatch(dsrSrc, dsrDb, matches, 1);

			double amount =  srcS.keypoints.get(0).size().height;
		//	System.out.println(amount+ loc.getName() + srcD.keypoints.get(0).size().height);
			//banany i jablka i pomidory i pilki
			if(amount<250&& (srcD.keypoints.get(0).size().height>400)){
				//System.out.println("kompletnie nie podobne");
				noSimilarVec.add(loc);
			}
			//trus i marchewki
			else if(amount>400 && ((srcD.keypoints.get(0).size().height)<250)){//28){)
				noSimilarVec.add(loc);
			}
			else{
				Double distResult =0.0;
				Double[] result = new Double[7];
				for(int j= 0 ; j<7; j++){

					List<DMatch> listD = new ArrayList<DMatch>();

					for(int i = 0; i<matches.size(); i++){
						listD = matches.get(i).toList();
					}

					Double dist = 0.0;
					for(DMatch d : listD){
						dist = dist + d.distance;
					}

					result[j]= (dist/srcD.keypoints.get(0).size().height);
				}
				Double pom = 0.0;
				for(int j = 0; j<7; j++)
					pom = pom+result[j];
				distResult =pom/7;  

		//		SaveDsr.save(loc, "sift", distResult);
				FileWeight fileW= new FileWeight(loc, distResult);

				similarImgs.add(fileW);

				/*
				Mat outImg = new Mat();

				Features2d.drawMatches(src, srcS.keypoints.get(0), dbMat,
						srcD.keypoints.get(0), matches.get(0), outImg,
						new Scalar(0, 255, 0), new Scalar(0, 0, 255), new MatOfByte(),
						Features2d.NOT_DRAW_SINGLE_POINTS);

				Features2d.drawMatches2(src, srcS.keypoints.get(0),resDb, srcD.keypoints.get(0), matches, outImg);
				 */
				//	DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(outImg));
				/*Mat outImage = new Mat();
				Mat outImage2 = new Mat();
				Features2d.drawKeypoints(src, srcS.keypoints.get(0), outImage);
				//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(outImage));

				Features2d.drawKeypoints(dbMat, srcD.keypoints.get(0), outImage2);
				//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(outImage2));
				 */
			}
		}

		similarVec = Sort.sort(similarImgs);

	//	System.out.println(similarImgs.size() + ", posegeregowne: ");

	//	for(File fi : similarVec)
	//		System.out.println(fi.getName());

		//?? mozna sprawdzic jeszcze
		int max = similarVec.size()-2;//- 4;
		if(noSimilarVec.size()<25&&noSimilarVec.size()>10)//&&similarImgs.size()<100)
			max = similarImgs.size() - 16;
		else if(noSimilarVec.size()<10&&noSimilarVec.size()>5)
			max = similarImgs.size() - 5;
		else if (similarImgs.size()>100)	
			max = similarImgs.size() - 26;
		//System.out.println("max: " + max + ", noSV : " + noSimilarVec.size());
		for(int j = (similarVec.size()-1); j> max; --j){	
			//System.out.println("i: " + i + ", "+ similarVec.get(i).getName());
			noSimilarVec.add(similarVec.get(j));
			similarVec.remove(j);
		}

	/*	//co odrzucam
		System.out.println("metoda sift co odpada:" );
		for(File f: noSimilarVec){
			System.out.println(f.getName());
		}
	*/	int b =0, c =0, j=0, p=0, pom=0, t =0; 
		for(int ji = 0; ji<noSimilarVec.size(); ji++){
			if(noSimilarVec.get(ji).getName().contains("ban")==true)
				b++;
			if(noSimilarVec.get(ji).getName().contains("carrot")==true)
				c++;
			if(noSimilarVec.get(ji).getName().contains("jab")==true)
				j++;

			if(noSimilarVec.get(ji).getName().contains("pilka")==true)
				p++;
			if(noSimilarVec.get(ji).getName().contains("pom")==true)
				pom++;
			if(noSimilarVec.get(ji).getName().contains("trus")==true)
				t++;
		}
		System.out.println("zosta�o: " + similarVec.size() + " metoda sift odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", p: " + p +", pom: " + pom +", t: " + t );
		
		return similarVec;
	}
}

