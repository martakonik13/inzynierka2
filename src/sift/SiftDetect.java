package sift;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

public class SiftDetect {

	public ArrayList<MatOfKeyPoint> keypoints = new ArrayList<MatOfKeyPoint>();
	
	//mam deskryptor 
	public Mat detect(Mat src){
		// do detekcji cech
		FeatureDetector siftFeatDetector = FeatureDetector
				.create(FeatureDetector.SIFT);
		//wyciagam cechy
		DescriptorExtractor siftDsrExtractor = DescriptorExtractor
				.create(DescriptorExtractor.SIFT); 
		
		keypoints.add(new MatOfKeyPoint());
		
		Mat dsr = new Mat();
		
		Mat grayMat = new Mat();
		Imgproc.cvtColor(src,  grayMat, Imgproc.COLOR_RGB2GRAY);
		siftFeatDetector.detect(grayMat, keypoints.get(0));
		siftDsrExtractor.compute(grayMat, keypoints.get(0), dsr);
		
		return dsr;
	}
}
