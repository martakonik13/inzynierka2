package viever;

import java.io.File;

public class FileWeight {
	public File file; 
	public Double weight;
	public Double weightR;
	public Double weightG;
	public Double weightB;
	
	public FileWeight(File fileGet, Double weightGet){
		file = fileGet;
		weight = weightGet;
		
	};
	public FileWeight(File fileGet, Double weightGetR, Double weightGetG, Double weightGetB){
		file = fileGet;
		weightR = weightGetR;
		weightG = weightGetG;
		weightB = weightGetB;
	};
}
