package viever.huMoments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.Mat;

public class FindHuMoments {

	public static Vector<Vector<Double> > find(Mat src) throws IOException{
		
		List<Mat> channel = new ArrayList<Mat>(3);
		Core.split(src,channel);
		Vector<Vector<Double> > dscImg = new Vector<Vector<Double> >(); 
		dscImg.addElement(FindOneChHuMoments.count(channel.get(0)));
		dscImg.addElement(FindOneChHuMoments.count(channel.get(1)));
		dscImg.addElement(FindOneChHuMoments.count(channel.get(2)));
		
		return dscImg;
	}
}
