package viever.huMoments;


import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;

import viever.FileWeight;
import viever.LoadImage;
import viever.Resizer;
import viever.Searcher;
import viever.Sort;
import viever.Moments.FindMoments;

public class HuMoments {

	public static Vector<File> hu (Mat srcMat) throws IOException{

		Vector<Vector<Double> > dscSrc = new Vector<Vector<Double> >(); 
		Vector<Vector<Double> > dscImg = new Vector<Vector<Double> >(); 
		File[] locations = Searcher.findLocations();
		
		//	DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(srcMat));
		
		FileWeight[] similarImgs = new FileWeight[locations.length];
		Vector<File> similarVec = new Vector<File>();
		Vector <File> noSimilarVec = new Vector<File>();

		dscSrc = FindHuMoments.find(srcMat);
		Mat dbMat = new Mat(); 
		Mat dbMatResize = new Mat();
		double dist= 0.0;
		int index = 0; 
		for( File loc: locations){
			dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(dbMat));
			//	System.out.println(loc.getAbsolutePath());
		//	dbMatResize = Resizer.changeSize(srcMat, dbMat);
			dscImg = FindHuMoments.find(dbMat);
		//	System.out.println("ile tych hu: " + dscImg.get(0).size());
			dist = CompareHu.compare(dscSrc, dscImg);
			similarImgs[index] = new FileWeight(loc,dist);
			index++;
		}
		similarVec = Sort.sort(similarImgs);
		
		System.out.println("posegeregowne: ");
		int in = 1; 
		for(File fi : similarVec){
			System.out.println(in + fi.getName());
			in ++;
		}
		int max = (similarVec.size()-13);// to jest dobre juz sprawdzone!
	
		//System.out.println("rozm:" + similarVec.size()+ ", -40: "+ (similarVec.size()-40));
		for(int i = (similarVec.size()-1); i> max; --i){
			//System.out.println("i: " + i + ", "+ similarVec.get(i).getName());
			noSimilarVec.add(similarVec.get(i));
			similarVec.remove(i);
		}	

		//wyswietlam co odrzycam: 
	/*	System.out.println("metoda HuMoments: " + noSimilarVec.size() );
		int in = 1; 
		for(File f: noSimilarVec){
			System.out.println(f.getName());
			in ++;
		}
	*/
		int b =0, c =0, j=0, m=0, p=0, pom=0, t =0; 
		for(int ji = 0; ji<noSimilarVec.size(); ji++){
			if(noSimilarVec.get(ji).getName().contains("ban")==true)
				b++;
			if(noSimilarVec.get(ji).getName().contains("carrot")==true)
				c++;
			if(noSimilarVec.get(ji).getName().contains("jab")==true)
				j++;
			if(noSimilarVec.get(ji).getName().contains("mini")==true)
				m++;
			if(noSimilarVec.get(ji).getName().contains("pilka")==true)
				p++;
			if(noSimilarVec.get(ji).getName().contains("pom")==true)
				pom++;
			if(noSimilarVec.get(ji).getName().contains("trus")==true)
				t++;
		}
		System.out.println("zosta�o: " + similarVec.size() + " huMoments odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", m: " + m +", p: " + p +", pom: " + pom +", t: " + t );
		
		
		
		return similarVec;
	}
}
