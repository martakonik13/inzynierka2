package viever.huMoments;
import java.util.Vector;

public class CompareHu {
	
	public static Double compare(Vector<Vector<Double> > dsrSrc, Vector<Vector<Double> >dsrImg ){
		
		Vector<Double> inDiff = new Vector<Double>();
		Double pom = 0.0; 
		//1chanel 
		//System.out.println("roznica 1chanel :");
		for(int i = 0; i<dsrImg.get(0).size(); i++){
			inDiff.add(Math.abs((dsrImg.get(0).get(i) - dsrSrc.get(0).get(i))/dsrSrc.get(0).get(i)));
			pom =+ inDiff.get(i);
			//System.out.println(inDiff.get(i));
		}
		//System.out.println(pom);
		pom=0.0;
		inDiff.clear();
		//2chanel 
		//System.out.println("roznica 2chanel :");
		for(int i = 0; i<dsrImg.get(0).size(); i++){
			inDiff.add(Math.abs((dsrImg.get(1).get(i) - dsrSrc.get(1).get(i))/dsrSrc.get(1).get(i)));
			pom =+ inDiff.get(i);
			//System.out.println(inDiff.get(i));
		}
		//System.out.println(pom);
		pom=0.0;
		inDiff.clear();
		//3chanel 
		//System.out.println("roznica 3chanel :");
		for(int i = 0; i<dsrImg.get(0).size(); i++){
			inDiff.add(Math.abs((dsrImg.get(2).get(i) - dsrSrc.get(2).get(i))/dsrSrc.get(2).get(i)));
			pom =+ inDiff.get(i);
			//System.out.println(inDiff.get(i));
		}	
		//System.out.println(pom);
		
		return pom;
	}
}
