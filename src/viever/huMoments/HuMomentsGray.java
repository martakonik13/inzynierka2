package viever.huMoments;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import viever.DisplayImage;
import viever.FileWeight;
import viever.LoadImage;
import viever.Mat2BufferedImage;
import viever.Resizer;
import viever.Searcher;
import viever.Sort;
import viever.Moments.FindOneChannelMoments;

public class HuMomentsGray {

	public static Vector<File> count(Mat srcMat, Vector<File> files) throws IOException{

		if(files.size()>25){
			Mat grayMat = new Mat();
			Vector<Double> inDiff = new Vector<Double>();

			File[] locations = Searcher.findLocations();
			Vector <File> similarVec = new Vector<File>();
			FileWeight[] similarImgs = new FileWeight[files.size()];

			Imgproc.cvtColor(srcMat,  grayMat, Imgproc.COLOR_RGB2GRAY);
			Vector<Double> dsrGray = FindOneChHuMoments.count(grayMat);
			Mat dbMat = new Mat(); 
			Mat resizeDbMat = new Mat();
			Mat dbMatGray = new Mat();
			Double pom = 0.0;
			int licz = 0; 
			int index = 0;
			for( File loc: files){
				dbMat = LoadImage.loadImage(loc.getAbsolutePath());
				//System.out.println(loc.getAbsolutePath());
				Imgproc.cvtColor(dbMat,  dbMatGray, Imgproc.COLOR_RGB2GRAY);
				resizeDbMat = Resizer.changeSize(srcMat, dbMatGray);

				//sprawdz czy mam juz deskryptor - dorobic
				//		System.out.println(dsrGray.get(2)+"   "+ FindOneChHuMoments.count(grayMat).get(2));
				Vector<Double> dsrImgGray = FindOneChHuMoments.count(resizeDbMat);
				pom=0.0;
				inDiff.clear();
				for(int i = 0; i<7; i++){
					Double odejmowanie = Math.abs((dsrImgGray.get(i) - dsrGray.get(i))/dsrGray.get(i));
				//	System.out.println(odejmowanie);
					//	if(odejmowanie<threshold)
					//		licz=licz+1;
					inDiff.add(odejmowanie);	

					//	System.out.println(inDiff.size()+" podzialaniu "+inDiff.get(i));
					pom =pom + inDiff.get(i);
				}
			//	System.out.println("suma:" + pom+", licz: " + licz);
				similarImgs[index] = new FileWeight(loc,pom);
				index++;

			}

			similarVec = Sort.sort(similarImgs);
			
			System.out.println("posegeregowne: ");
			int in = 1; 
			for(File fi : similarVec){
				System.out.println(in + fi.getName());
				in ++;
			}
			
			Vector <File> noSimilarVec = new Vector<File>();
	/*		
			int max= (similarVec.size()-5);
			//System.out.println("rozm:" + similarVec.size()+ ", -40: "+ (similarVec.size()-40));
			for(int i = (similarVec.size()-1); i> max; --i){
				//System.out.println("i: " + i + ", "+ similarVec.get(i).getName());
				noSimilarVec.add(similarVec.get(i));
				similarVec.remove(i);
			}	
	/*		
			//wyswietlam co odrzycam: 
			System.out.println("metoda HuMoments: " + noSimilarVec.size() );
			for(File f: noSimilarVec)
				System.out.println(f.getName());
	*/
		/*	
			int b =0, c =0, j=0, p=0, pomi=0, t =0; 
			for(int ji = 0; ji<noSimilarVec.size(); ji++){
				if(noSimilarVec.get(ji).getName().contains("ban")==true)
					b++;
				if(noSimilarVec.get(ji).getName().contains("carrot")==true)
					c++;
				if(noSimilarVec.get(ji).getName().contains("jab")==true)
					j++;
				if(noSimilarVec.get(ji).getName().contains("pilka")==true)
					p++;
				if(noSimilarVec.get(ji).getName().contains("pom")==true)
					pomi++;
				if(noSimilarVec.get(ji).getName().contains("trus")==true)
					t++;
			}
			System.out.println("zosta�o: " + similarVec.size() + " huMomGray odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", p: " + p +", pom: " + pomi +", t: " + t );
			
			*/
			return similarVec;
		}
		else 
			return files;
	}
}
