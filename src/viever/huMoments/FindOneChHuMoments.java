package viever.huMoments;

import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;


public class FindOneChHuMoments {
	
	public static Vector<Double> count (Mat chn ) throws IOException{
		
		Vector<Double> dscImg = new Vector<Double>();
		Mat hu = new Mat();
		Moments m = new Moments();
		m = Imgproc.moments(chn, false);
		Imgproc.HuMoments(m, hu);
		//System.out.println("rows: "+ hu.rows() + ",  cols: " + hu.cols());
		for(int i = 0; i<hu.rows(); i++){
			for(int j = 0; j<hu.cols(); j++){
			//	System.out.println((hu.get(i, j)[0]));
				dscImg.addElement(hu.get(i, j)[0]);
			}
		}
		//System.out.println(dscImg.get(2).toString());
		return dscImg;
	}

}
