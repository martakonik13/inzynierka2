package viever;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;

public class MatchContur {

	public static void contur(Mat src) throws IOException{
		
		Mat binSrc = makeBin(src);
		
		ArrayList<MatOfPoint> contoursSrc = new ArrayList<MatOfPoint>();
		
		Imgproc.findContours(binSrc, contoursSrc , new Mat(),Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);
				
		File[] loc = Searcher.findLocations();
		Mat img = LoadImage.loadImage(loc[0].getAbsolutePath());
	
		DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(img));
		
		Mat binImg = makeBin(img);
		ArrayList<MatOfPoint> contoursImg = new ArrayList<MatOfPoint>();
		Imgproc.findContours(binImg, contoursImg , new Mat(),Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);
		
		System.out.println(Imgproc.matchShapes(binSrc, binImg, Imgproc.CV_CONTOURS_MATCH_I1, 0));
		
	}
	public static Mat makeBin(Mat src) throws IOException{
		
		Mat grayMat = new Mat();
		
		Imgproc.cvtColor(src, grayMat, Imgproc.COLOR_RGB2GRAY);
		System.out.println("po pierwszym");
		
		Resizer.changeSize(src, grayMat);
	//	Mat bin = new Mat(src.rows(),src.cols(),src.type());
		
	//	Imgproc.threshold(grayMat,bin,170,255,Imgproc.THRESH_BINARY);
		//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(bin));
		
		
		return grayMat;
	}
	
}
