package viever;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;


public class FoundImg extends JFrame {
	
	/**
	 * Launch the application.
	 */
	public static void Show(final Vector<File> imgs, final String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FoundImg frame = new FoundImg(imgs,name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param imgs 
	 * @throws IOException 
	 */
	public FoundImg(Vector<File> imgs, String name) throws IOException {
		setTitle("Wyszukane obrazy metoda" + name);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(406, 1200);
		
		JPanel panel = new JPanel();
		JScrollPane scrollPane = new JScrollPane(panel);
		panel.setLayout(new BoxLayout(panel, getDefaultCloseOperation()));
		
		
		for(File f : imgs){
			BufferedImage img = ImageIO.read(f);
			Image img2 = img.getScaledInstance(224,224 ,Image.SCALE_DEFAULT);
			JLabel lab = new JLabel();
			JLabel labName = new JLabel(f.getName());
			panel.add(labName);
			lab.setIcon(new ImageIcon(img2));
			panel.add(lab);
			
			//panel.add(new JButton("Przycisk 2"));
			}
		
		getContentPane().add(scrollPane);
	
		setVisible(true);
	}

}
