package viever;

import java.io.File;
import java.util.Vector;

public class MixRGBiORB {

	public static Vector<File> met(Vector<File> rgbVec, Vector<File> huVec ){

		Vector<File> similarImg = new Vector<File>();
		Vector<FileWeight> similar = new Vector<FileWeight>(); 
		Vector<FileWeight> similar2 = new Vector<FileWeight>(); 
		Vector<FileWeight> similarOst = new Vector<FileWeight>();
		
		double pom = 0; 
		
		for(int i = rgbVec.size()-1;i>0;i-- ){
			File f = rgbVec.get(i);
			FileWeight temp = new FileWeight(f,pom);
			similar.add(temp);
			pom ++;
		}
		pom = 0; 
		for(int i = huVec.size()-1;i>0;i-- ){
			File f = huVec.get(i);
			FileWeight temp = new FileWeight(f,pom);
			similar2.add(temp);
			pom ++;
		}
		//lacze
		pom = 0;
		for(int i = 0; i<similar.size(); i++){
			for(int j = 0; j<similar2.size(); j++){
				if(similar.get(i).file.getName().contains(similar2.get(j).file.getName())){
				//	System.out.println("dodaje:" + similar.get(i).file.getName()+ similar2.get(j).file.getName() );
					pom = similar.get(i).weight + similar2.get(j).weight;
					FileWeight temp = new FileWeight(similar.get(i).file,pom);
					similarOst.addElement(temp);
				}
			}
		}
		
		similarImg = Sort.sort(similarOst);
		
		System.out.println("posegeregowne mix: ");
		int in = 1; 
		for(File fi : similarImg){
			System.out.println(in + fi.getName());
			in ++;
		}
		
		return similarImg;
	}


}
