package viever;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import viever.Moments.Compare1Channel;
import viever.Moments.CompareImage;
import viever.Moments.FindMoments;
import viever.Moments.FindOneChannelMoments;

public class MomentsGray {

	public static Vector <File> hist(File src, Double threshold) throws IOException{
		
		Mat srcMat = LoadImage.loadImage(src.getAbsolutePath());
		Mat grayMat = new Mat();
		Vector<Double> dsrGray = new Vector<Double>(); 
		Vector<Double> inDiff = new Vector<Double>();
		Vector<Double> inDiffAvg = new Vector<Double>();
		Vector<Double> dsrImgGray = new Vector<Double>();
		
		File[] locations = Searcher.findLocations();
		Vector <File> similarVec = new Vector<File>();
		FileWeight[] similarImgs = new FileWeight[locations.length];
		
		Imgproc.cvtColor(srcMat,  grayMat, Imgproc.COLOR_RGB2GRAY);
		DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(grayMat));
		dsrGray = FindOneChannelMoments.findMoments(grayMat);
		Mat dbMat = new Mat(); 
		Mat resizeDbMat = null;
		Mat dbMatGray = new Mat();
		int index = 0; 
		Double pom = 0.0;
		
		for( File loc: locations){
			dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			System.out.println(loc.getAbsolutePath());
			Imgproc.cvtColor(dbMat,  dbMatGray, Imgproc.COLOR_RGB2GRAY);
			//zrobic rozmiar taki jak rozmiar obrazka src
			resizeDbMat = Resizer.changeSize(srcMat, dbMatGray);
			DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(resizeDbMat));
			//sprawdz czy mam juz deskryptor - dorobic
			dsrImgGray = FindOneChannelMoments.findMoments(resizeDbMat);
	
			for(int i = 0; i<dsrImgGray.size(); i++){
				inDiff.add(Math.abs((dsrImgGray.get(i) - dsrGray.get(i))/dsrGray.get(i)));
				pom = pom + inDiff.get(i);
				//	System.out.println(inDiff.get(i));
			}
			FileWeight fileW= new FileWeight(loc, pom/(7.0));
			similarImgs[index] = fileW;
			index++;
		}
		similarVec = Sort.sort(similarImgs);
		return similarVec;
			
		/*	//licze srednia rzedow 
			inDiffAvg.add((inDiff.get(0)+inDiff.get(1))/2);
			inDiffAvg.add((inDiff.get(2)+inDiff.get(3)+inDiff.get(4))/3);
			inDiffAvg.add((inDiff.get(5)+inDiff.get(6)+inDiff.get(7)+inDiff.get(8))/4);
			
			System.out.println("�rednie:");
			for(double i: inDiffAvg){
				System.out.println(i);
			}
			if(inDiffAvg.get(0)< 0.569)
				if(inDiffAvg.get(1)< 0.8) //0.8
					if(inDiffAvg.get(2)< 2.5){ //0.856
						similarImgs.add(loc);
						System.out.println("podobne!");
						}
			inDiff.clear();
			inDiffAvg.clear();*/
			
		}
	//	for(File d : similarImgs)
		//	 System.out.println(d.getAbsolutePath());
		
	//	if(Compare1Channel.Count(dsrGray, threshold)>10)
		//	return true;
		
	}

