package viever;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class ColorToChannel {

	public static List<Mat> colorToChannels(Mat img) throws IOException{
		
		List<Mat> channel = new ArrayList<Mat>(3);
		Core.split(img,channel);
	
		Mat fin_img = new Mat();		
	    Mat g = Mat.zeros(img.rows(),img.cols(),CvType.CV_8UC1 );
	    
	    List<Mat> channels = new ArrayList<Mat>(3);
	    
	    //red
	    channels.add(g);
	    channels.add(g);
	    channels.add(channel.get(2));
		
		Core.merge(channels, fin_img);
		//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(fin_img));
		
		//green
		channels.clear();
		channels.add(g);
		channels.add(channel.get(1));
		channels.add(g);
	    
	    
	    Core.merge(channels, fin_img);
		//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(fin_img));
	    
	    //blue
	    channels.clear();
	    channels.add(channel.get(0));
	    channels.add(g);
	  	channels.add(g);
	  	
		
	  	Core.merge(channels, fin_img);
		//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(fin_img));
		
	  	
		return channel;
	}
}
