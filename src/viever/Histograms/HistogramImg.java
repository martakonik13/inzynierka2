package viever.Histograms;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import viever.DisplayImage;
import viever.LoadImage;
import viever.Mat2BufferedImage;
import viever.Searcher;
import viever.Moments.FindMoments;

public class HistogramImg {

	public static Vector<Mat> makeHist(Mat srcMat) throws IOException{
	
	Vector<Mat> histograms = new Vector<Mat> ();	
	List<Mat> channel = new ArrayList<Mat>(3);
	Core.split(srcMat,channel);

	//histogram 
	List<Mat> matList = new ArrayList<Mat>();
	
	//blue
	matList.clear();
	matList.add(channel.get(0)); //blue - 0 , green - 1, red - 2
	Mat hist = new Mat();
	MatOfFloat ranges = new MatOfFloat(0,256);
	MatOfInt histSize = new MatOfInt(255);
	Imgproc.calcHist(matList, new MatOfInt(0), new Mat(), hist , histSize, ranges);

	// Create space for histogram image
	Mat histImage = Mat.zeros( 100, (int)histSize.get(0, 0)[0], CvType.CV_8UC1);
	// Normalize histogram                          
	Core.normalize(hist, hist, 1, histImage.rows() , Core.NORM_MINMAX, -1, new Mat() );   
	// Draw lines for histogram points
	for( int i = 0; i < (int)histSize.get(0, 0)[0]; i++ )
	{                   
	   Core.line(histImage,new org.opencv.core.Point( i, histImage.rows() ),new org.opencv.core.Point( i, histImage.rows()-Math.round( hist.get(i,0)[0] )) ,new Scalar( 255, 255, 255),1, 8, 0 );
	}
//	DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(histImage));
	histograms.add(hist);
	//green
	matList.clear();
	matList.add(channel.get(1)); //blue - 0 , green - 1, red - 2
	Mat hist2 = new Mat();
	Imgproc.calcHist(matList, new MatOfInt(0), new Mat(), hist2 , histSize, ranges);

	// Create space for histogram image
		Mat histImage2 = Mat.zeros( 100, (int)histSize.get(0, 0)[0], CvType.CV_8UC1);
	// Normalize histogram                          
	Core.normalize(hist2, hist2, 1, histImage2.rows() , Core.NORM_MINMAX);//, -1, new Mat() );   
	// Draw lines for histogram points
	for( int i = 0; i < (int)histSize.get(0, 0)[0]; i++ )
	{                   
	   Core.line(histImage2,new org.opencv.core.Point( i, histImage2.rows() ),new org.opencv.core.Point( i, histImage2.rows()-Math.round( hist2.get(i,0)[0] )) ,new Scalar( 255, 255, 255),1, 8, 0 );
	}
//	DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(histImage2));
	histograms.add(hist2);
	//red
	matList.clear();
	matList.add(channel.get(2)); //blue - 0 , green - 1, red - 2
	Mat hist3 = new Mat();
	Imgproc.calcHist(matList, new MatOfInt(0), new Mat(), hist3 , histSize, ranges);

	// Create space for histogram image
	Mat histImage3 = Mat.zeros( 100, (int)histSize.get(0, 0)[0], CvType.CV_8UC1);
	// Normalize histogram                          
	Core.normalize(hist3, hist3, 1, histImage3.rows() , Core.NORM_MINMAX, -1, new Mat() );   
	// Draw lines for histogram points
	for( int i = 0; i < (int)histSize.get(0, 0)[0]; i++ )
	{                   
	   Core.line(histImage3,new org.opencv.core.Point( i, histImage3.rows() ),new org.opencv.core.Point( i, histImage3.rows()-Math.round( hist3.get(i,0)[0] )) ,new Scalar( 255, 255, 255),1, 8, 0 );
	}
//	DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(histImage3));
	histograms.add(hist3);
	
	return histograms;
	}
		
}
	