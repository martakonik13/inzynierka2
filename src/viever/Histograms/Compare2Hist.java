package viever.Histograms;

import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
//porownuje korelacje  - srednia
public class Compare2Hist {
//porownuje srednia wszystkich 6 histogramow
	public static Double compare(Vector<Mat> histSrc, Vector<Mat> histImg, Double threshold){
		
		Vector<Double> diffCorr = new Vector<Double>();
		Vector<Double> diffChisqr = new Vector<Double>();
		Vector<Double> diffIntersect = new Vector<Double>();
		Vector<Double> diffBhat = new Vector<Double>();
		Vector<Double> diffHell = new Vector<Double>();
		
		for(int i = 0 ; i<histSrc.size(); i++){
			diffCorr.add(Imgproc.compareHist(histSrc.get(i),histImg.get(i), Imgproc.CV_COMP_CORREL));	
			//System.out.println(diffCorr.get(i));
		}
		System.out.println("sumCor: " + (diffCorr.get(0)+diffCorr.get(1)+diffCorr.get(2)));
		System.out.println("avgCor: " + (diffCorr.get(0)+diffCorr.get(1)+diffCorr.get(2))/3);
		/*
		for(int i = 0 ; i<histSrc.size(); i++){
			diffChisqr.add(Imgproc.compareHist(histSrc.get(i),histImg.get(i), Imgproc.CV_COMP_CHISQR));	
			//System.out.println(diffChisqr.get(i));
		}
		System.out.println("sumChi: " + (diffChisqr.get(0)+diffChisqr.get(1)+diffChisqr.get(2)));
		System.out.println("avgChi: " + (diffChisqr.get(0)+diffChisqr.get(1)+diffChisqr.get(2))/3);
		
		for(int i = 0 ; i<histSrc.size(); i++){
			diffIntersect.add(Imgproc.compareHist(histSrc.get(i),histImg.get(i), Imgproc.CV_COMP_INTERSECT));	
			//System.out.println(diffCorr.get(i));
		}
		System.out.println("sumInt: " + (diffIntersect.get(0)+diffIntersect.get(1)+diffIntersect.get(2)));
		System.out.println("avgInt: " + (diffIntersect.get(0)+diffIntersect.get(1)+diffIntersect.get(2))/3);
		
		for(int i = 0 ; i<histSrc.size(); i++){
			diffBhat.add(Imgproc.compareHist(histSrc.get(i),histImg.get(i), Imgproc.CV_COMP_BHATTACHARYYA));	
			//System.out.println(diffCorr.get(i));
		}
		System.out.println("sumBha: " + (diffBhat.get(0)+diffBhat.get(1)+diffBhat.get(2)));
		System.out.println("avgBha: " + (diffBhat.get(0)+diffBhat.get(1)+diffBhat.get(2))/3);
		
		for(int i = 0 ; i<histSrc.size(); i++){
			diffHell.add(Imgproc.compareHist(histSrc.get(i),histImg.get(i), Imgproc.CV_COMP_HELLINGER));	
			//System.out.println(diffCorr.get(i));
		}
		System.out.println("sumhell: " + (diffHell.get(0)+diffHell.get(1)+diffHell.get(2)));
		System.out.println("avghell: " + (diffHell.get(0)+diffHell.get(1)+diffHell.get(2))/3);
		
		*/
		
	//	if(((diffCorr.get(0)+diffCorr.get(1)+diffCorr.get(2))/3)>threshold)
	//		return true;

		
		return ((diffCorr.get(0)+diffCorr.get(1)+diffCorr.get(2))/3)-1;
	}
}
