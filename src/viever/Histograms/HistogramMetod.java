package viever.Histograms;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;

import viever.DisplayImage;
import viever.FileWeight;
import viever.LoadImage;
import viever.Mat2BufferedImage;
import viever.Resizer;
import viever.Searcher;
import viever.Sort;
import viever.Moments.CompareImage;
import viever.Moments.FindMoments;

public class HistogramMetod {

	public static Vector<File> compare(File src, Double threshold) throws IOException{

		Mat srcMat = LoadImage.loadImage(src.getAbsolutePath());
		Vector<Mat> histSrc = new Vector<Mat>();
		histSrc = HistogramImg.makeHist(srcMat);
		
		//petla dla wszystkich plikow z bazy, licze deskryptory i porownuje czy pasuja
		Mat dbMat, resizeDbMat = null;
		
		File[] locations = Searcher.findLocations();
		Vector <File> similarVec = new Vector<File>();
		FileWeight[] similarImgs = new FileWeight[locations.length];
		int index = 0;
		Double dist = 0.0;
		
		for( File loc: locations){
			dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			System.out.println(loc.getAbsolutePath());
			//zrobic rozmiar taki jak rozmiar obrazka src
			resizeDbMat = Resizer.changeSize(srcMat, dbMat);
			//sprawdz czy mam juz histogram - dorobic
			//if(
			dist = Compare2Hist.compare(histSrc, HistogramImg.makeHist(resizeDbMat), threshold);
				//similarImgs.add(loc);
				//System.out.println("podobne!");
			similarImgs[index] = new FileWeight(loc,dist);
			index++;
			
		}
	
	/*	for(File im :similarImgs)
	    {
	       System.out.println(im.getAbsolutePath());
	    }*/
		similarVec = Sort.sort(similarImgs);
		
		return similarVec;	
	}
	
}

