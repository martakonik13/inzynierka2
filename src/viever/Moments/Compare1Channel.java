package viever.Moments;

import java.util.Vector;

public class Compare1Channel {

	
	public static int Count(Vector<Double> inDiff, Double threshold ){
		
		int num=0; 
		//double threshold = 0.80 ;
		for (int i = 0; i<2; i++){
			if(inDiff.get(i)<0.2)
				num=num+2;
			//else if (inDiff.get(i)<2*threshold)
			//	num=num+2;
			else if(inDiff.get(i)<threshold)
				num=num+1;
			//else if (inDiff.get(i)<1.5)
				//num=num+1;
			else if(inDiff.get(i)>1.5)
				num = num - 1;
			else if(inDiff.get(i)>4)
				num = num - 2;
			else if(inDiff.get(i)>30.0) 
				num = num - 7;
			else if(inDiff.get(i)>40.0) 
				num = num - 10;
		}
		for (int i = 2; i<5; i++){
			if(inDiff.get(i)<0.2)
				num=num+4;
			//else if (inDiff.get(i)<2*threshold)
			//	num=num+2;
			else if(inDiff.get(i)<threshold)
				num=num+3;
			else if (inDiff.get(i)<1.5)
				num=num+1;
			else if(inDiff.get(i)>1.5)
				num = num - 1;
			else if(inDiff.get(i)>4)
				num = num - 2;
			else if(inDiff.get(i)>30.0) 
				num = num - 7;
			else if(inDiff.get(i)>40.0) 
				num = num - 10;
		}
		for (int i = 5; i<inDiff.size(); i++){
			if(inDiff.get(i)<0.2)
				num=num+3;
			//else if (inDiff.get(i)<2*threshold)
			//	num=num+2;
			else if(inDiff.get(i)<(0.8*threshold))
				num=num+2;
			else if (inDiff.get(i)<1.5)
				num=num+1;
			else if(inDiff.get(i)>1.4)
				num = num - 2;
			else if(inDiff.get(i)>2.5)
				num = num - 4;
			else if(inDiff.get(i)>30.0) 
				num = num - 7;
			else if(inDiff.get(i)>40.0) 
				num = num - 10;
		}
		System.out.println("num: " +num);
		
		return num;
		
		
	}
}
