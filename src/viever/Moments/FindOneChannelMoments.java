package viever.Moments;

import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

public class FindOneChannelMoments {
	//podaje 1 kanal i licze momenty
	public static Vector<Double> findMoments( Mat chn ) throws IOException
	{
	Vector<Double> dscImg = new Vector<Double>();
	Moments m = new Moments();
	m = Imgproc.moments(chn, false );
	
	//momenty centralne 1 rzedu = 0 :( w pracy blad? 
	// 1 rzedu momenty zwykle! (spatial)
	
//	System.out.println("m01: " + m.get_m01());
	dscImg.add(m.get_m01());
//	System.out.println("m10: " + m.get_m10());
	dscImg.add(m.get_m10());
	//2 rzad
//	System.out.println("mu02: " + m.get_mu02());
	dscImg.add(m.get_mu02());
//	System.out.println("mu11: " + m.get_mu11());
	dscImg.add(m.get_mu11());
//	System.out.println("mu20: " + m.get_mu20());
	dscImg.add(m.get_mu20());
	//3 rzad
//	System.out.println("mu03: " + m.get_mu03());
	dscImg.add(m.get_mu03());
//	System.out.println("mu30: " + m.get_mu30());
	dscImg.add(m.get_mu30());
//	System.out.println("mu12: " + m.get_mu12());
	dscImg.add(m.get_mu12());
//	System.out.println("mu21: " + m.get_mu21());
	dscImg.add(m.get_mu21());
	
	return dscImg;
	}
	
}
