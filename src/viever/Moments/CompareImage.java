
package viever.Moments;

import java.io.IOException;
import java.util.Vector;

public class CompareImage {
//dostaje desryptory i porownuje ze soba  - licze sednie momentow danego rzedu danego koloru, potem srednia z rzedow momentow
//mam 3 wagi dla 3 kolorow 
 public static Double compareImages(Vector<Vector<Double> > src, Vector<Vector<Double> > dbImage) throws IOException{
	 
	 Vector<Double> inDiff = new Vector<Double>();
	 Double pom, wR, wG,wB = new Double(0);
	 double sum = 0.0;
	// double threshold = 0.8 ;
	 inDiff.clear();
	
	 //czerwony - roznica wzgledna 9-ciu deskryptorow
	// System.out.println("------------W-R-------------");	
	 for(int i = 0; i<src.get(0).size(); i++){   	
    	pom = Math.abs(((dbImage.get(0).elementAt(i))-src.get(0).elementAt(i))/src.get(0).elementAt(i));
		 inDiff.add(pom);
	 }
	 //zlicz ile spelnia warunek
	//if(Compare1Channel.Count(inDiff, threshold)<5)
      //  	return false;   
	 for(Double d : inDiff)
		 sum = sum +d;
    inDiff.clear();
    
    //zielony
    //System.out.println("------------W-G-------------");
    for(int i = 0; i<src.get(1).size(); i++){   	
    	inDiff.addElement(Math.abs((dbImage.get(1).elementAt(i)-src.get(1).elementAt(i))/src.get(1).elementAt(i))) ;
    	//System.out.println(Math.abs((dbImage.get(1).elementAt(i)-src.get(1).elementAt(i))/src.get(1).elementAt(i))) ;
    }
    for(Double d : inDiff)
		 sum = sum +d;
    
    //zlicz ile spelnia warunek
   	//if(Compare1Channel.Count(inDiff, threshold)<5)
      //     	return false;   
   		 
     inDiff.clear();
     
     //niebieski
    // System.out.println("------------W-B-------------");
    for(int i = 0; i<src.get(2).size(); i++){   	
   	 	inDiff.addElement(Math.abs((dbImage.get(2).elementAt(i) - src.get(2).elementAt(i))/src.get(2).elementAt(i))) ;
   	 //	System.out.println(Math.abs((dbImage.get(2).elementAt(i) - src.get(2).elementAt(i))/src.get(2).elementAt(i))) ;
    }
    
    for(Double d : inDiff)
		 sum = sum +d;
    
  //zlicz ile spelnia warunek
  // 	if(Compare1Channel.Count(inDiff, threshold)<5)
    //       	return false;   
   		 
    inDiff.clear();

/*
    if (((wR<0.58)&&(wG<0.58))||((wR<0.58)&&(wB<0.58))||(wB<0.58)&&(wG<0.58))
    	if((wR<1.0)&&(wG<1.0)&&(wB<1.0))
    		return true;
  */
    return sum;
 }
 public static Double averages(Vector<Double> inDiff){
	Double avg1 = (inDiff.get(0)+inDiff.get(1))/2;
	 // System.out.println(avg1);
	Double avg2 = (inDiff.get(2)+inDiff.get(3)+inDiff.get(4))/3;
	  //System.out.println(avg2);
	Double avg3 = (inDiff.get(5)+inDiff.get(6)+inDiff.get(7)+inDiff.get(8))/4;
	  //System.out.println(avg3);
	return (avg1+avg2+avg3)/3; 
 }
}