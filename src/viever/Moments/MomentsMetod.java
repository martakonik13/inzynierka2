package viever.Moments;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import viever.DisplayImage;
import viever.FileWeight;
import viever.LoadImage;
import viever.Mat2BufferedImage;
import viever.Resizer;
import viever.Searcher;
import viever.Sort;

public class MomentsMetod {

	public static Vector<File> getSimilar(File src) throws IOException{
		
		File[] locations = Searcher.findLocations();
		Vector<File> similarVec = new Vector<File>();
		FileWeight[] similarImgs = new FileWeight[locations.length];
		Vector <File> noSimilarVec = new Vector<File>();

		System.out.println(src.getName());
		
		Mat srcMat = LoadImage.loadImage(src.getAbsolutePath());
		//dscSrc wszystkie 3x9 momentow zrodla 
		Vector<Vector<Double> > dscSrc = FindMoments.findMoments(srcMat);

		//petla dla wszystkich plikow z bazy, licze deskryptory i porownuje czy pasuja
		Mat dbMat, resizeDbMat = null;
		int index = 0; 
		double dist= 0.0;
		
		for( File loc: locations){
			dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			//System.out.println(loc.getAbsolutePath());
			dist = CompareImage.compareImages(dscSrc,FindMoments.findMoments(dbMat));
			similarImgs[index] = new FileWeight(loc,dist);
			index++;
			}
		
		similarVec = Sort.sort(similarImgs);
		
		int in =1;
/*	System.out.println("posegeregowne: ");
		for(File fi : similarVec){
			System.out.println(in + fi.getName());
			in++;
		}
	*/	int max = similarVec.size() - 10;//(int)(1000/similarVec.size()+4);

		for(int i = (similarVec.size()-1); i> max; --i){
			noSimilarVec.add(similarVec.get(i));
			similarVec.remove(i);
		}
		//wyswietlam co odrzycam: 
		System.out.println("metoda Moments: " + noSimilarVec.size() );
		for(File f: noSimilarVec)
			System.out.println(f.getName());
	
		int b =0, c =0, j=0, m=0, p=0, pom=0, t =0; 
		for(int ji = 0; ji<noSimilarVec.size(); ji++){
			if(noSimilarVec.get(ji).getName().contains("ban")==true)
				b++;
			if(noSimilarVec.get(ji).getName().contains("carrot")==true)
				c++;
			if(noSimilarVec.get(ji).getName().contains("jab")==true)
				j++;
			if(noSimilarVec.get(ji).getName().contains("mini")==true)
				m++;
			if(noSimilarVec.get(ji).getName().contains("pilka")==true)
				p++;
			if(noSimilarVec.get(ji).getName().contains("pom")==true)
				pom++;
			if(noSimilarVec.get(ji).getName().contains("trus")==true)
				t++;
		}
		System.out.println("zosta�o: " + similarVec.size() + " huMoments odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", m: " + m +", p: " + p +", pom: " + pom +", t: " + t );
		
	
		
		
		return similarVec;

		//Mat dbImage = LoadImage.loadImage("C:/Users/marta/workspace/viever/src/viever/images/jablkoprawe.jpg");

		//if(CompareImage.compareImages(src, dbImage)==true)
		//System.out.println("S� podobne!");

	}
}
