package viever.Moments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import viever.ColorToChannel;

public class FindMoments {
	//podaje RGB i tu dziele na kanaly i licze wszystkie momenty
	public static Vector<Vector<Double> > findMoments( Mat Img ) throws IOException
	{
	Vector<Vector<Double> > dscImg = new Vector<Vector<Double> >();
	List<Mat> channel = new ArrayList<Mat>(3);
	Core.split(Img,channel);//ColorToChannel.colorToChannels(Img);
	//czerwony
	 dscImg.addElement(FindOneChannelMoments.findMoments(channel.get(0)));
    //zielony
    dscImg.addElement(FindOneChannelMoments.findMoments(channel.get(1)));
	//niebieski
    dscImg.addElement(FindOneChannelMoments.findMoments(channel.get(2)));
   
   
    return dscImg;

	}
}
