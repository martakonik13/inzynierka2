package viever;

import java.io.File;
import java.util.Vector;

public class Sort {
	//od najmniejszego do najwiekszego
	public static Vector<File> sort(FileWeight[] similarImgs){
		Vector <File> similarVec = new Vector<File>();

		for(int k = 0; k<similarImgs.length; k++){
			for(int j = 0; j<similarImgs.length-1; j++){
					if(similarImgs[j].weight>similarImgs[j+1].weight){
						FileWeight temp = similarImgs[j];
						similarImgs[j] = similarImgs[j+1];
						similarImgs[j+1]= temp;
					
				}
			}
		}

		for(FileWeight f:similarImgs)
			similarVec.addElement(f.file);
		
	/*	System.out.println("posegeregowne: ");
		
		for(File fi : similarVec)
			System.out.println(fi.getName());
*/
		return similarVec;
	}
	public static Vector<File> sort(Vector<FileWeight> similarImgs){
		Vector <File> similarVec = new Vector<File>();
		
		for(int k = 0; k<similarImgs.size(); k++){
			for(int j = 0; j<similarImgs.size()-1; j++){
					if(similarImgs.get(j).weight>similarImgs.get(j+1).weight){
						FileWeight temp = similarImgs.get(j);
						similarImgs.set(j, similarImgs.get(j+1));
						similarImgs.set(j+1, temp);
				}
			}
		}
		for(FileWeight f:similarImgs)
			similarVec.addElement(f.file);
		
		return similarVec;
	}
	public static Vector<File> sort(FileWeight[] similarImgs, char channel){
		
		Vector <File> similarVec = new Vector<File>();
		if(channel == 'R'){
			for(int k = 0; k<similarImgs.length; k++){
				for(int j = 0; j<similarImgs.length-1; j++){
					if(similarImgs[j].weightR>similarImgs[j+1].weightR){
						FileWeight temp = similarImgs[j];
						similarImgs[j] = similarImgs[j+1];
						similarImgs[j+1]= temp;
					}
				}
			}

			for(FileWeight f:similarImgs)
				similarVec.addElement(f.file);
			
			//System.out.println("posegeregowne wzgl R: ");
			
			//for(File fi : similarVec)
				//System.out.println(fi);
		}
		if(channel == 'G'){
			for(int k = 0; k<similarImgs.length; k++){
				for(int j = 0; j<similarImgs.length-1; j++){
					if(similarImgs[j].weightG>similarImgs[j+1].weightG){
						FileWeight temp = similarImgs[j];
						similarImgs[j] = similarImgs[j+1];
						similarImgs[j+1]= temp;
					}
				}
			}

			for(FileWeight f:similarImgs)
				similarVec.addElement(f.file);
			
			//System.out.println("posegeregowne wzgl G: ");
			
			//for(File fi : similarVec)
				//System.out.println(fi);
		}
		if(channel == 'B'){
			for(int k = 0; k<similarImgs.length; k++){
				for(int j = 0; j<similarImgs.length-1; j++){
					if(similarImgs[j].weightB>similarImgs[j+1].weightB){
						FileWeight temp = similarImgs[j];
						similarImgs[j] = similarImgs[j+1];
						similarImgs[j+1]= temp;
					}
				}
			}

			for(FileWeight f:similarImgs)
				similarVec.addElement(f.file);
			
			//System.out.println("posegeregowne wzgl B: ");
			
			//for(File fi : similarVec)
				//System.out.println(fi);
		}
		return similarVec;
	}
}
