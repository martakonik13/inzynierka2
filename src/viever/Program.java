package viever;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import Binary.BinaryImageMethod;
import Binary.BinaryImg;
import Brief.BriefMet;
import ORB.OrbMet;
import sift.SiftMethod;
import viever.BOX.Ibox2Img;
import viever.BOX.Ibox8;
import viever.BOX.RGBbox;
import viever.Histograms.HistogramMetod;
import viever.Moments.MomentsMetod;
import viever.huMoments.HuMoments;
import viever.huMoments.HuMomentsGray;

public class Program {

	public  Vector<File> metRGBbox;
	public  Vector<File> metIbox;
	public  Vector<File> metGrayBox;
	public  Vector<File> metHuMoments; 
	public  Vector<File> metHuMomentsGray; 
	public  Vector<File> metHuMomentsRGB;
	public  Vector<File> metMomentsRGB;
	public  Vector<File> metHistRGB;
	public  Vector<File> metMomentsGray;
	public  Vector<File> metBinaryImageMethod;
	public  Vector<File> metSift;
	public  Vector<File> metBrief;
	public  Vector<File> metOrb;
	public  Vector<File> mixRGBiORB;
	
	public static Program context (File src) throws IOException{
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
	
		//Program.metBinaryImageMethod = BinaryImageMethod.met(srcMat);
		
			//System.out.println(src.getName());
			
			Mat srcMat = LoadImage.loadImage(src.getAbsolutePath());	
	
			Program prog = new Program();
		//	prog.metMomentsRGB = MomentsMetod.getSimilar(src, 0.0);
		//	prog.metHuMomentsRGB = HuMoments.hu(srcMat);  // - do dupy 
		//	prog.metMomentsRGB = MomentsMetod.getSimilar(src); // - odrzuca do 5 dorych 
			
			prog.metSift = SiftMethod.met(srcMat);//, prog.metBrief); //obcinam co 'kompletnie nie pasuja'
			prog.metBrief = BriefMet.met(srcMat,prog.metSift); //obcinam co 'kompletnie nie pasuja'
		//	prog.metBinaryImageMethod = BinaryImageMethod.met(srcMat, prog.metSift);// - nieskuteczne 
		//	prog.metRGBbox = RGBbox.rgb(srcMat, prog.metBinaryImageMethod); 
			//prog.metHuMomentsGray = HuMomentsGray.count(srcMat,prog.metBrief); // nie wiem czy polepsza
		//	
			prog.metOrb = OrbMet.met(srcMat, prog.metBrief);
			prog.metRGBbox = RGBbox.rgb(srcMat, prog.metOrb); //
		//  prog.metHuMomentsGray = HuMomentsGray.count(srcMat,prog.metRGBbox); - nie dzala
		//	prog.metIbox = Ibox2Img.box(srcMat, prog.metRGBbox); - nie dziala :(
		//	prog.mixRGBiORB = MixRGBiORB.met(prog.metRGBbox, prog.metHuMomentsGray);
			
			return prog;
	}
}
