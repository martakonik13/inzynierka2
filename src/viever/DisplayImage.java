package viever;

import java.awt.FlowLayout;
import java.awt.Image;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class DisplayImage {

	 public static void displayImage(Image img2) throws IOException
	 {   
	     //BufferedImage img=ImageIO.read(new File("jablko.jpg"));
	     ImageIcon icon=new ImageIcon(img2);
	    try{
	     JFrame frame=new JFrame();
	     frame.setLayout(new FlowLayout());        
	     frame.setSize(img2.getWidth(null)+50, img2.getHeight(null)+50);     
	     JLabel lbl=new JLabel();
	     lbl.setIcon(icon);
	     frame.add(lbl);
	     frame.setVisible(true);
	     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    }catch(Exception e){}
	 }
}
