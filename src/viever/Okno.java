package viever;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.opencv.core.Core;

import viever.Histograms.HistogramImg;
import viever.Histograms.HistogramMetod;
import viever.Moments.MomentsMetod;

import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JSplitPane;
import java.awt.Color;
import javax.swing.JScrollPane;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;



public class Okno extends JFrame {

	private JPanel contentPane;
	JButton btnNewButton = new JButton("Otw�rz");
	private final JLabel labelFoto = new JLabel("");
	
	public static void main(String[] args) throws IOException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Okno frame = new Okno();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	public Okno() throws IOException {
		setResizable(false);
		setTitle("Obraz Wzorcowy");
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
				contentPane.setLayout(new BorderLayout(0, 0));
				contentPane.add(btnNewButton, BorderLayout.NORTH);
				btnNewButton.setBackground(Color.WHITE);
				
				
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						btnOtworzActionPerformed(e);
					}
				});
				
						btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
						
						contentPane.add(labelFoto, BorderLayout.CENTER);
						contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{btnNewButton}));
	
	}
	public void btnOtworzActionPerformed(ActionEvent e) {
		try{
			JFileChooser jfc = new JFileChooser();
			if(jfc.showOpenDialog(btnNewButton)==JFileChooser.APPROVE_OPTION){
				System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
				File file = jfc.getSelectedFile(); 
				BufferedImage bi = ImageIO.read(file);
				ImageIcon imgIcon = new ImageIcon(bi);
				labelFoto.setIcon(imgIcon);
				//Searcher.findLocations();
				
				File[] loc = Searcher.findLocations();
				
		//pierwsze 20 - banany
				for(int i = 0; i<20; i++){
					System.out.println("-----------------------------------------------------------------------------------"+ i);
					System.out.println(loc[i].getName());
					Program prog = Program.context(loc[i]);
	//				FoundImg.Show(prog.metRGBbox, loc[i].getName());
				}
	/*
				//20-40 - marchewki
				for(int i = 20; i<39; i++){
					System.out.println("-----------------------------------------------------------------------------------"+ i);
					Program prog = Program.context(loc[i]);
					System.out.println(loc[i].getName());
				//	FoundImg.Show(prog.metRGBbox, loc[i].getName());
			}
		/*	
				//40-60 - jablka
				for(int i = 39; i<59; i++){
				System.out.println("-----------------------------------------------------------------------------------"+ i);
				Program prog = Program.context(loc[i]);
				FoundImg.Show(prog.metRGBbox, loc[i].getName());
			}
	/*
				//60-80 - pilki
					for(int i = 59; i<79; i++){
				System.out.println("-----------------------------------------------------------------------------------"+ i);
				System.out.println(loc[i].getName());
				Program prog = Program.context(loc[i]);
			//	FoundImg.Show(prog.metRGBbox, loc[i].getName());
			}
	/*
				//80-100 - pomidory
				for(int i = 79; i<99; i++){
				System.out.println("-----------------------------------------------------------------------------------"+ i);
				System.out.println("obrazek: " + loc[i].getName());
				Program prog = Program.context(loc[i]);
			//	FoundImg.Show(prog.metRGBbox, loc[i].getName());
			}
	/*
				//100-120 - truskawki
				for(int i = 99; i<118; i++){
				System.out.println("-----------------------------------------------------------------------------------"+ i);
				System.out.println("obrazek: " + loc[i].getName());
				Program prog = Program.context(loc[i]);
			//	FoundImg.Show(prog.metRGBbox, loc[i].getName());
	}
	*/
				
				//metoda sredniej pixeli w calym obrazie
			//	FoundImg.Show(Program.metBinaryImageMethod," metPix");
				//metoda momentow centralnych 
			//	FoundImg.Show(Program.metMomentsRGB, "momenty");
				//IBOX - szary
			//	FoundImg.Show(Program.metGrayBox," GrayBox");
				//momenty hu  szary
			//	FoundImg.Show(Program.metHuMoments," Hu gray");
				//momenty hu rgb - suma roznic wzglednych 
			//	FoundImg.Show(Program.metHuMomentsRGB, " Hu rgb");
				//histogramy rgb 
				//FoundImg.Show(Program.metHistRGB, " Histogram"); - cos popsulam
				//FoundImg.Show(Program.metMomentsGray, "momenty centralne szary");
		
				//metoda brief
				//FoundImg.Show(prog.metBrief, "BRIEF");
				//metoda sift 
				//FoundImg.Show(prog.metSift, "SIFT");
				
	//			Program prog = Program.context(file);
				//metoda RGBBOX8 - srednia 
			//	FoundImg.Show(prog.metRGBbox,"rgbBOX" );	
			
		System.out.println("koniec");
			
			}
		}catch(IOException ex) {
		}
		}
  
}










