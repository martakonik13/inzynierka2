package viever.BOX;

import org.opencv.core.Mat;

public class CompareIBox {

	public static Double compare(Mat src, Mat img){
		double dist = 0;
		for (int i = 0; i< src.cols(); i++){
			for(int j = 0; j<src.rows(); j++){
				//System.out.print(src.get(i,j)[0]+ " ");
				dist = dist + Math.abs(src.get(i,j)[0] - img.get(i,j)[0]);
			}
			//System.out.println("");
		}
	//	System.out.println("roznica" + dist);	
		return dist;
	}
}
