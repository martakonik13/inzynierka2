package viever.BOX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.Mat;

public class RGBboxSplit {

	public static Vector<Mat> makeBoxes(Mat img) throws IOException{
		
		List<Mat> channel = new ArrayList<Mat>(3);
		Core.split(img,channel);
		Vector<Mat> boxes = new Vector<Mat>();
		
		//ch 1 
		boxes.add(Ibox8.gray(channel.get(0)));
		//ch 2
		boxes.add(Ibox8.gray(channel.get(1)));
		//ch 3 
		boxes.add(Ibox8.gray(channel.get(2)));
		
		return boxes;
	}
}
