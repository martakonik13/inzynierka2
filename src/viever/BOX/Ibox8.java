package viever.BOX;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import viever.LoadImage;

public class Ibox8 {
	
	public static Mat gray (Mat grayMat) throws IOException{
			
		//Size sz = new Size(8,8);
		Size sz = new Size(16,16);
		Imgproc.resize(grayMat, grayMat, sz);
	
		//DisplayImage.displayImage(Mat2BufferedImage.changeMat2BufferedImage(grayMat));
		
	//	for (int i = 0; i< grayMat.cols(); i++){
	//		for(int j = 0; j<grayMat.rows(); j++)
	//			System.out.print(grayMat.get(i,j)[0]+ " ");
	//		System.out.println("");
	//	}
	//	System.out.println("");
		return grayMat;
	}
	
}
