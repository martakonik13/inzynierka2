package viever.BOX;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import viever.FileWeight;
import viever.LoadImage;
import viever.Searcher;
import viever.Sort;

public class RGBbox {

	public static Vector <File> rgb(Mat srcMat,Vector<File> files) throws IOException{
		Double threshold = 0.0;
		
		Vector<Mat> srcVecMat = new Vector<Mat>();
 		//File[] locations = Searcher.findLocations();
 		Vector <File> similarVec = new Vector<File>();
 		
		FileWeight[] similarImgs = new FileWeight[files.size()];
		FileWeight[] similarImgsAvg = new FileWeight[files.size()];
		srcVecMat = RGBboxSplit.makeBoxes(srcMat);
		int i = 0; 
		Double pom = 0.0;
		Mat dbMat = new Mat();
		for( File loc: files){
			dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			
			//System.out.println(loc.getAbsolutePath());
			Vector<Mat> imgVecMat = RGBboxSplit.makeBoxes(dbMat);
			
			
			Double[] dist = CompareVecIBox.vec(srcVecMat,imgVecMat, threshold);
			pom= (dist[0]+dist[1]+dist[2])/3;
			FileWeight fileW = new FileWeight(loc,dist[0],dist[1],dist[2]);
			FileWeight fileWAvg = new FileWeight(loc,pom);
			similarImgs[i] = fileW;
			similarImgsAvg[i] = fileWAvg;
			i++;
		}
	//	similarVec = Sort.sort(similarImgs,'R');
	//	similarVec = Sort.sort(similarImgs,'B');
	//	similarVec = Sort.sort(similarImgs,'G');
		
		similarVec = Sort.sort(similarImgsAvg);
		
		

		Vector<File> noSimilarVec = new Vector<File>();
		int max = similarVec.size(); 
		if(similarVec.size()>40&&similarVec.size()<50)
			max = similarVec.size() - 11;
		else if(similarVec.size()>50&&similarVec.size()<60)	
			max = similarVec.size() - 21;
		else if(similarVec.size()>60&&similarVec.size()<70)	
			max = similarVec.size() - 31;
		else if(similarVec.size()>70)
			max = similarVec.size() - 41;

		for(int j = (similarVec.size()-1); j> max; --j){	
			noSimilarVec.add(similarVec.get(j));
			similarVec.remove(j);
		}
		
	//wyswietlam co odrzycam: 
		System.out.println("metoda rgb: " + noSimilarVec.size() );
		
		int b =0, c =0, j=0, p=0, pomi=0, t =0; 
		for(int ji = 0; ji<noSimilarVec.size(); ji++){
			if(noSimilarVec.get(ji).getName().contains("ban")==true)
				b++;
			if(noSimilarVec.get(ji).getName().contains("carrot")==true)
				c++;
			if(noSimilarVec.get(ji).getName().contains("jab")==true)
				j++;
			if(noSimilarVec.get(ji).getName().contains("pilka")==true)
				p++;
			if(noSimilarVec.get(ji).getName().contains("pom")==true)
				pomi++;
			if(noSimilarVec.get(ji).getName().contains("trus")==true)
				t++;
		}
		System.out.println("zosta�o: " + similarVec.size() + " rgb odpada : " + noSimilarVec.size() +", b: " + b +", c: " + c +", j: " + j +", p: " + p +", pom: " + pomi +", t: " + t );
		
		System.out.println("posegeregowne RGB: ");

		int in = 1; 
		for(File fi : similarVec){
			System.out.println(in + fi.getName());
			in ++;
		}
		
		return similarVec;
	}
	
}
