package viever.BOX;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import viever.FileWeight;
import viever.LoadImage;
import viever.Searcher;
import viever.Sort;

public class Ibox2Img {

 	public static Vector<File> box(Mat src, Vector<File> files) throws IOException{
 	
 		File[] locations = Searcher.findLocations();
		Vector <File> similarVec = new Vector<File>();
		FileWeight[] similarImgs = new FileWeight[files.size()];
		Mat grayMat = new Mat();
		Imgproc.cvtColor(src,  grayMat, Imgproc.COLOR_RGB2GRAY);
	
		Mat srcMatBox = Ibox8.gray(grayMat);
		Mat dbMat = new Mat();
		Mat grayDb = new Mat();
		Double dist = 0.0; 
		int i = 0;
		for( File loc: files){
			dbMat = LoadImage.loadImage(loc.getAbsolutePath());
			Imgproc.cvtColor(dbMat,grayDb  , Imgproc.COLOR_RGB2GRAY);
		//	System.out.println(loc.getAbsolutePath());
			Mat dbMatBox = Ibox8.gray(grayDb);
			dist = CompareIBox.compare(srcMatBox,dbMatBox);
		//	System.out.println("dist: "+ dist);
			FileWeight fileW= new FileWeight(loc, dist);
			similarImgs[i] = fileW;
			i++;
		}
		similarVec = Sort.sort(similarImgs);
 		
		System.out.println("Ibox posegeregowne: ");
		int in = 1; 
		for(File fi : similarVec){
			System.out.println(in + fi.getName());
			in ++;
		}
		
		return similarVec;
 	}
 	
 	
}
