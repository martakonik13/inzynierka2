package viever.BOX;

import java.util.Vector;

import org.opencv.core.Mat;

public class CompareVecIBox {

	public static Double[] vec(Vector<Mat> src, Vector<Mat> img, Double threshold ){

		double i=0, j=0, k=0;
		Double[] dist = new Double[3];
		i = CompareIBox.compare(src.get(0),img.get(0));
		j = CompareIBox.compare(src.get(1),img.get(1));
		k = CompareIBox.compare(src.get(2),img.get(2));

		dist[0]=i;
		dist[1]=j;
		dist[2]=k;
	//	System.out.println("ch1: " + i);
	//	System.out.println("ch2: " + j);
	//	System.out.println("ch3: " + k);
	//	System.out.println("razem: " + (i+j+k));
	
		return dist;
	}
}
