package viever;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class Resizer {

	public static Mat changeSize(Mat src, Mat img){
		
		Mat resizeImg = new Mat();
		Size sz = new Size( src.width(),(src.width()*img.height())/img.width());
		Imgproc.resize(img, resizeImg, sz);
		
		return resizeImg;
	}
}
